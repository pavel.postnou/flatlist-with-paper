import { StyleSheet, StatusBar } from "react-native";

export const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    input: {
      color: "#555555",
      width: "97%",
      marginVertical: 10,
      paddingRight: 10,
      paddingLeft: 10,
      borderColor: "grey",
      borderWidth: 3,
      borderRadius: 5,
      alignSelf: "center",
      backgroundColor: "#ffffff",
    },
    allContainer: {
      marginTop: StatusBar.currentHeight || 0,
      flex: 1,
      justifyContent: "center",
      backgroundColor: "#c4c4c4",
    },
    toogleView:{
      flexDirection:"row",
      alignItems:"center",
      justifyContent:"space-evenly"
    },
    toogle:{
      flex:1
    },
    badge:{
      position:"absolute",
      top:100,
      right:20,
      backgroundColor:"black"
    }

  });