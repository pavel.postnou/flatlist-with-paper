import { View, ScrollView } from "react-native";
import React from "react";
import Item from "../../components/Item";
import { styles } from "./favoriteScreen.style";
import Context from "../../context/context";

export default function favoriteScreen() {
  const context = React.useContext(Context);
  let users = context.userState.user;

  return (
    <View style={styles.allContainer}>
      <ScrollView style={styles.container}>
        {users && users.length > 0
          ? users.map((user) => {
              return <Item data={user} key={user.id} />;
            })
          : null}
      </ScrollView>
    </View>
  );
}
