import React from "react";
import { StyleSheet } from "react-native";
import ImageComponent from "../../src/shared/components/Image";
import { Banner } from "react-native-paper";

export default function errorScreen(props) {
  return (
    <Banner
      style={styles.errorView}
      visible={true}
      actions={[]}
      icon={({}) => (
        <ImageComponent
          type="errorImg"
          source={require("../../assets/error.png")}
        />
      )}
    >
      {props.route.params.message}
    </Banner>
  );
}

export const styles = StyleSheet.create({
  errorView: {
    height: 150,
    alignItems: "center",
  },
});
