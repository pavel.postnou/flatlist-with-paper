import { FlatList, SafeAreaView, View} from "react-native";
import {
  ActivityIndicator,
  Searchbar,
  Switch,
  Snackbar,
  Badge,
} from "react-native-paper";
import * as RootNavigation from "../../navigation/RootNavigation";
import React, { useState, useEffect } from "react";
import Item from "../../components/Item";
import refreshFetch from "../../api/fetchRequest";
import { styles } from "./mainScreen.style";
import TextComponent from "../../src/shared/components/Text";

export default function mainScreen() {
  let key = 0;
  let found = 0;
  let listItem = [];
  const [dark, setTheme] = useState(true);
  const [count, setCount] = useState(0);
  const [flatListItem, setFlatlistItem] = useState([]);
  const [search, setSearch] = useState("");
  const [users, setUsers] = useState(null);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = React.useState(false);

  async function refresh() {
    setLoading(true);
    setUsers(null);
    const result = await refreshFetch();
    result
      ? (setUsers(result), setLoading(false), setVisible(true))
      : (setLoading(false),
        RootNavigation.navigate("Error", { message: "Something went wrong" }));
  }

  useEffect(() => {
    refresh();
  }, []);

  function add() {
    for (let i = 0; i < users.results.length; i++) {
      listItem.push({
        number:key,
        id: users.results[i].login.uuid,
        name: users.results[i].name.first,
        image: users.results[i].picture.large,
        lastName: users.results[i].name.last,
        country: users.results[i].location.country,
        city: users.results[i].location.city,
        date: users.results[i].registered.date.substring(0, 10),
      });
      key++;
    }
    setFlatlistItem(listItem);
  }

  useEffect(() => {
    users ? add() : null;
  }, [users]);

  const renderItem = ({ item }) =>
    (item && item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1) ||
    item.lastName.toLowerCase().indexOf(search.toLowerCase()) !== -1
      ? (found++,
        setCount(found),
        (<Item data={item} users={users} theme={dark} />))
      : null;

  const onToggleSwitch = () => setTheme(!dark);

  const onDismissSnackBar = () => setVisible(false);
  return (
    <View style={styles.allContainer}>
      {!loading ? (
        <SafeAreaView style={styles.container}>
          <Searchbar
            style={styles.input}
            inputStyle={styles.input}
            placeholder="Search"
            onChangeText={(text) => setSearch(text)}
            value={search}
            iconColor="red"
          />
          <FlatList
            data={flatListItem}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            refreshing={false}
            onRefresh={refresh}
          />
          <View style={styles.toogleView}>
            <TextComponent type="nameText" theme={dark} text="Изменить тему" />
            <Switch
              style={styles.toogle}
              color="yellow"
              value={dark}
              onValueChange={onToggleSwitch}
            />
          </View>
          <Snackbar
            visible={visible}
            onDismiss={onDismissSnackBar}
            action={{
              label: "OK",
            }}
          >
            Загрузка завершена
          </Snackbar>
          <Badge size={30} style={styles.badge}>
            found {count} users
          </Badge>
        </SafeAreaView>
      ) : (
        <ActivityIndicator size={100} color="blue" />
      )}
    </View>
  );
}
