import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    user: {
      flex: 1,
      height: "100%",
      backgroundColor: "#2e2e2e",
      marginVertical: 10,
      marginHorizontal: 10,
      borderRadius: 10,
    },
    userLight: {
      flex: 1,
      height: "100%",
      backgroundColor: "#999999",
      marginVertical: 10,
      marginHorizontal: 10,
      borderRadius: 10,
    },
    imgAndName: {
      flex: 1,
      flexDirection: "row",
    },
    allInfo: {
      flex: 2,
      padding: 10,
    },
    imgOne: {
      flex: 1,
      resizeMode: "contain",
      marginVertical: 10,
      marginLeft:10,
      borderRadius: 10,
    },
    name: {
      flex: 1,
      alignItems: "center",
      marginVertical: 10,
    },
  });