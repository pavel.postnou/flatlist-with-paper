import { View } from "react-native";
import React from "react";
import { styles } from "./describeScreen.style";
import TextComponent from "../../src/shared/components/Text";
import ImageComponent from "../../src/shared/components/Image";

export default function describeScreen(props) {
  let {
    theme,
    send: { name },
    send: { picture },
    send: { dob },
    send: { gender },
    send: { location },
    send: { email },
    send: { phone },
  } = props.route.params;

  return (
    <View style={theme ? styles.user : styles.userLight}>
      <View style={styles.imgAndName}>
        <ImageComponent type="imgDescription" source={{ uri: picture.large }} />
        <View style={styles.name}>
          <TextComponent type={"describe"} theme={theme} text="Name: " />
          <TextComponent type={"nameText"} theme={theme} text={name.first} />
          <TextComponent type={"describe"} theme={theme} text="Last Name: " />
          <TextComponent type={"nameText"} theme={theme} text={name.last} />
          <TextComponent type={"describe"} theme={theme} text="Age: " />
          <TextComponent type={"nameText"} theme={theme} text={dob.age} />
        </View>
      </View>
      <View style={styles.allInfo}>
        <TextComponent
          type={"description"}
          theme={theme}
          text={`Hello, i am ${gender}, i live in ${location.street.name} ${
            location.street.number
          } city ${location.city}, state ${location.state}, in ${
            location.country
          }. I am ${dob.age} age old. My date of birth is ${dob.date.slice(
            0,
            10
          )}. I will be glad to talk.`}
        />
        <TextComponent type={"descriptionRed"} theme={theme} text="email: " />
        <TextComponent type={"description"} theme={theme} text={email} />
        <TextComponent type={"descriptionRed"} theme={theme} text="phone: " />
        <TextComponent type={"description"} theme={theme} text={phone} />
      </View>
    </View>
  );
}
