import { StyleSheet} from "react-native";
import { theme } from "../mainDarkTheme";

export const darkStyle = StyleSheet.create({
    user: {
      flex:1,
      backgroundColor: theme.colors.primary,
      marginBottom: 5,
      marginHorizontal: 5,
      borderRadius: 10,
    },
    title: {
      fontSize: theme.titleSize,
      color:theme.colors.title,
      marginLeft:20
    },
    subtitle:{
      fontSize: theme.subtitleSize,
      color:theme.colors.subtitle,
      marginLeft:20
    },
    leftCardStyle:{
      width:"30%"
    },
    cardStyle:{
      height:100
    },
    imgOne: {
      flex: 1,
      width: 80,
      resizeMode: "contain",
      marginVertical: 5,
      borderRadius: 10,
    },
    infoContainer: {
      paddingLeft: 10,
      width: "90%",
    },
    cityContainer: {
      flex: 1,
      flexDirection:"row",
      backgroundColor:theme.colors.accent,
      borderBottomEndRadius:10,
      borderBottomStartRadius:10
    },
    naming: {
      color: theme.colors.naming,
      fontSize: theme.namingSize,
      alignSelf: "flex-start",
      marginLeft:10
    },
    devider: {
      borderBottomColor: "#999999",
      borderBottomWidth: 5,
      width: "85%",
    },
    date: {
      fontSize: theme.dateSize,
      color: "#a11818",
    },
    country: {
      flex:1,
      marginTop:10,
      alignItems:"center"
    },
    surface: {
      height:30,
      marginTop:8,
      backgroundColor:theme.colors.primary
    },

  });
  