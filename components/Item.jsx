import { View } from "react-native";
import { Surface, Card, Title, IconButton } from "react-native-paper";
import React from "react";
import * as RootNavigation from "../navigation/RootNavigation";
import { darkStyle } from "./Item.dark.style";
import { lightStyle } from "./item.light.style";
import ImageComponent from "../src/shared/components/Image";
import Context from "../context/context";

function Item({ data, users, theme }) {
  const context = React.useContext(Context);
  const [pressed, setPressed] = React.useState(true);
  const [icon, setIcon] = React.useState("heart-outline");
  let styles;
  let length;
  let user = context.userState.user;

  async function press() {
    user.push(data);
    length = user.length;
    await context.handleSetData({ user, length });
    setPressed(!pressed);
    pressed ? setIcon("heart") : setIcon("heart-outline");
  }

  theme ? (styles = darkStyle) : (styles = lightStyle);
  const goInfo = () => {
    let send;
    users
      ? ((send = users.results[data.number]),
        RootNavigation.navigate("Describe", { send, theme }))
      : null;
  };
  const LeftContent = () => (
    <ImageComponent type="imgOne" source={{ uri: data.image }} />
  );

  const RigthContent = () => (
    <IconButton icon={icon} color={"red"} size={20} onPress={() => press()} />
  );
  return data ? (
    <Card onPress={goInfo} style={styles.user} elevation={0}>
      <Card.Title
        style={styles.cardStyle}
        titleStyle={styles.title}
        subtitleStyle={styles.subtitle}
        leftStyle={styles.leftCardStyle}
        title={data.lastName}
        subtitle={data.name}
        left={LeftContent}
        right={RigthContent}
      />
      <Card.Content style={styles.cityContainer}>
        <View style={styles.country}>
          <View style={styles.cityContainer}>
            <Title style={styles.naming}>country: </Title>
            <Title style={styles.naming}>{data.country}</Title>
          </View>
          <View style={styles.cityContainer}>
            <Title style={styles.naming}>city: </Title>
            <Title style={styles.naming}>{data.city}</Title>
          </View>
        </View>
        <View style={styles.country}>
          <View style={styles.cityContainer}>
            <Title style={styles.naming}>registered: </Title>
            <Surface style={styles.surface}>
              <Title style={styles.date}>{data.date}</Title>
            </Surface>
          </View>
        </View>
      </Card.Content>
    </Card>
  ) : null;
}

export default React.memo(Item);
