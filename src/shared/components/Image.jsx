import { Image} from "react-native";
import React from "react";
import { styles } from "./image.style";
import { Avatar } from "react-native-paper";

export default function ImageComponent({ type, source,...rest }) {
  if (type == "imgDescription") return <Avatar.Image style = {styles[type]} source={source} size={190}/>
  else return <Image style = {styles[type]} source={source}/>
}
