import { Text } from "react-native";
import React from "react";
import { darkStyles } from "./text.dark.style";
import { lightStyles } from "./text.light.style";

export default function TextComponent({ type, text, theme, ...rest }) {
  let styles;
  theme ? (styles = darkStyles) : (styles = lightStyles);
  return (
    <Text style={styles[type]}>{text}</Text>
  );
}
