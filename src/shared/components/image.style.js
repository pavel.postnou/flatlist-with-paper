import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
  imgDescription: {
    marginVertical: 10,
    marginLeft: 10,
  },
  imgOne: {
    height: 90,
    resizeMode: "contain",
    borderRadius: 5,
    marginLeft: -40,
  },
  errorImg: {
    width:100,
    height:100,
    resizeMode: "contain",
  },
});
