import { StyleSheet } from "react-native";

export const darkStyles = StyleSheet.create({
  title: {
    fontSize: 15,
    color: "white",
    marginRight: 20,
  },
  country: {
    fontSize: 15,
    color: "#57abff",
    marginRight: 20,
  },
  naming: {
    flex:1,
    color: "yellow",
    fontSize: 12,
    alignSelf: "flex-start",
  },
  date: {
    fontSize: 10,
    color: "white",
  },
  nameText: {
    fontSize: 30,
    color: "white",
  },
  describe: {
    color: "yellow",
    fontSize: 15,
  },
  description: {
    color: "white",
    fontSize: 17,
  },
  descriptionRed: {
    color: "red",
    fontSize: 17,
  },
  error: {
    color:"red",
    fontSize: 25,
  }
});
