import {SET_USER} from './types'
export const defaultState = {
    user:[]
}

export const ReducerFunction =  (state = defaultState, action) => {
    switch (action.type) {
        case SET_USER:
            let newState = {
                ...state,...action.data
            }
            return newState;
        default:
            return state;
    }
};
