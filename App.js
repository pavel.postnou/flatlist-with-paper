import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import describeScreen from "./screens/describeScreen";
import errorScreen from "./screens/errorScreen/errorScreen";
import { navigationRef } from "./navigation/RootNavigation";
import { Provider as PaperProvider } from "react-native-paper";
import { theme } from "./mainLightTheme";
import bottomComponent from "./navigation/bottomNavigation";
import Context from "./context/context";
import * as ACTIONS from "./actions/users.action"
import { ReducerFunction, defaultState } from "./context/reducer";

const Stack = createStackNavigator();

export default function App() {
  const [stateUser, dispatchUserReducer] = React.useReducer(ReducerFunction, defaultState);
  const handleSetData = (data) => {
    dispatchUserReducer(ACTIONS.setUser(data));
  };


  return (
    <PaperProvider theme={theme}>
      <Context.Provider value={{
      userState: stateUser,
      handleSetData: (data) => handleSetData(data)
    }}>

      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator>
          <Stack.Screen
            name="Main"
            component={bottomComponent}
            options={{ headerShown: false }}
          />
          <Stack.Screen name="Describe" component={describeScreen} />
          <Stack.Screen name="Error" component={errorScreen} />
        </Stack.Navigator>
      </NavigationContainer>
      </Context.Provider>
    </PaperProvider>
  );
}
