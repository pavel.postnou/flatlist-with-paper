export default async function refreshFetch() {
    try {
  const response = await fetch("https://randomuser.me/api/?results=150");
  let result = await response.json()
  return response ? result : false;
    }   
    catch(e) {
        throw(e)
    }
}