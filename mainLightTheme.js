import {DefaultTheme} from "react-native-paper";

export const theme = {
    ...DefaultTheme,
    titleSize:25,
    subtitleSize:20,
    namingSize:15,
    dateSize:10,
    colors: {
      ...DefaultTheme.colors,
      primary:"white",
      accent:"lightgrey",
      title:"blue",
      subtitle:"#666ff2",
      naming: "#757575",
      date:"#a11818"
    },
    
  };