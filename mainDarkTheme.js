import {DarkTheme} from "react-native-paper";

export const theme = {
    ...DarkTheme,
    titleSize:25,
    subtitleSize:20,
    namingSize:15,
    dateSize:10,
    flex: 1,
      height: "100%",
      backgroundColor: "#2e2e2e",
      marginVertical: 10,
      marginHorizontal: 10,
      borderRadius: 10,
    colors: {
      ...DarkTheme.colors,
      primary:"rgba(128, 128, 128, 0.5)",
      accent:"black",
      title:"black",
      subtitle:"#d6cb38",
      naming: "#d6cb38",
      date:"#a11818"
    },
    
  };