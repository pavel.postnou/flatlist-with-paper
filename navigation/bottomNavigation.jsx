import * as React from "react";
import { BottomNavigation } from "react-native-paper";
import mainScreen from "../screens/mainScreen";
import favoriteScreen from "../screens/favoriteScreen";
import Context from "../context/context";

const bottomComponent = () => {
  const context = React.useContext(Context);
  let user = context.userState.user;
  const [index, setIndex] = React.useState(0);
  const [routes, setRoutes] = React.useState([
    { key: "users", title: "Users", icon: "account-group" },
    { key: "favorite", title: "Favorite", icon: "account-heart", badge: 0 },
  ]);

  React.useEffect(() => {
    user && user.length > 0
      ? setRoutes([
          { key: "users", title: "Users", icon: "account-group" },
          {
            key: "favorite",
            title: "Favorite",
            icon: "account-heart",
            badge: user.length,
          },
        ])
      : setRoutes([
          { key: "users", title: "Users", icon: "account-group" },
          {
            key: "favorite",
            title: "Favorite",
            icon: "account-heart",
            badge: 0,
          },
        ]);
  }, [context.userState.length]);
 

  const renderScene = BottomNavigation.SceneMap({
    users: mainScreen,
    favorite: favoriteScreen,
  });
  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
      activeColor="red"
      inactiveColor="blue"
      barStyle={{ backgroundColor: "black" }}
    />
  );
};

export default bottomComponent;
